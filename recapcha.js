$(document).ready(function () {
  $(".forgot-pass-btn").click(function (e) {
    clearErrors();
    e.preventDefault();
    let siteKey = "";

    $.ajax({
      type: "POST",
      url: ajaxscript.ajax_url,
      data: {
        action: "get_recaptcha_site_key_for_js",
      },
      success: function (response) {
        siteKey = response;
        grecaptcha.ready(function () {
          grecaptcha.execute(siteKey, {action: 'forgotps'}).then(function (token) {
            //send request for reset password
            send_request($(this).attr("id"), token);
          });
        });
      }
    });
  });
});



