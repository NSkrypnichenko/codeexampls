function processPagination(e) {
  e.preventDefault();
  let paramsInput = $('.search-form .search-field');

  let pressedPage = $(this).attr('data-linkpage');
  let searchParam = paramsInput.attr('data-searchval');

  let categoryParam = paramsInput.attr('data-catval');
  let pageParams = $('.content').attr('data-categoryname');

  categoryParam = categoryParam ? categoryParam : pageParams;
  let filterBy = $('.content').attr('data-filterBy');

  // if need to update url
  // document.title = titleCat;
  //history.pushState({page_title: titleCat}, titleCat, linkCat);

  ajaxPaginator(pressedPage, searchParam, categoryParam, filterBy);
}

var ajaxscript = {ajax_url: '/wp-admin/admin-ajax.php'}

/**
 * send parameters to back to show correct page. pagination understand filtering
 *
 * @param pressedPage
 * @param searchParam
 * @param categoryParam
 * @param filterBy
 */
function ajaxPaginator(pressedPage, searchParam, categoryParam, filterBy) {
  let $mainBox = $('.content');
  $mainBox.animate({opacity: 0.5}, 300);
  $.ajax({
    type: "POST",
    url: ajaxscript.ajax_url,
    data: {
      action: 'pagination',
      paged: pressedPage,
      search_string: searchParam,
      link: categoryParam,
      filterBy: filterBy
    },
    success: function (response) {
      $('.content')
        .html(response)
        .animate({opacity: 1}, 300);
      $('.pagination .nav-links a').click(processPagination);
    },
    error: function (response) {
      $('.content')
        .animate({opacity: 1}, 300);
    }
  });
}

$(document).ready(function () {
  $('.pagination .nav-links a').click(processPagination);
});
