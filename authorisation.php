<?php

/**
 * registration function. Get params from $POST array. send all fields to backend for check if all correct.
 * if response is ok - return success to js, if response is not ok - return response. if no response -
 * redirect to page with 500 error.
 */
function show_registration()
{
  session_start();

  $params = array(
    'first_name' => $_POST['user_first_name'],
    'last_name' => $_POST['user_last_name'],
    'password' => $_POST['user_pass'],
    'email' => $_POST['user_email'],
    'subscribed' => $_POST['subscribed'],
    'api_key' => get_for_rubi_api_key(),
    'scope' => "user"
  );
  if (isset($_POST['tel'])) {
    $params += array('phone' => $_POST['tel']);
  }

  $url = get_for_rubi_api_url() . '/incoming/register';

  $result = api_get_as_json($params, $url);
  $json = json_decode($result);
  if ($json->status) {
    echo "success";
  } else {
    if (isset($json->status)) {
      echo $result;
    } else {
      echo get_home_url() . "/500/";
    }
  }
  wp_die();
}
add_action('wp_ajax_show_registration', 'show_registration');
add_action('wp_ajax_nopriv_show_registration', 'show_registration');

/**
 * send request for url with params
 *
 * @param $params array with field
 * @param $url string request url
 * @return bool|string
 */
function api_get_as_json($params, $url)
{
  //url-ify the data for the POST
  $fields_string = http_build_query($params);

  //open connection
  $ch = curl_init();

  //set the url, number of POST vars, POST data
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

  //So that curl_exec returns the contents of the cURL; rather than echoing it
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  //execute post
  return curl_exec($ch);
}

