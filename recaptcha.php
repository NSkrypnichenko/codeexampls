<?php
/**
 * private key and public key can be changed from admin panel, if need
 *
 * @return mixed getter for recaptcha site key
 */
add_action('wp_ajax_get_recaptcha_site_key_for_js', 'get_recaptcha_site_key_for_js');
add_action('wp_ajax_nopriv_get_recaptcha_site_key_for_js', 'get_recaptcha_site_key_for_js');

function get_recaptcha_site_key_for_js()
{
  echo get_custom_option('theme_options', "site_key");
  wp_die();
}


