<?php
$wp_query = new WP_Query(defining_parameters());

if ($wp_query->have_posts()) :
  while ($wp_query->have_posts()) :
    the_post();
    get_template_part('template-parts/section-items/content');
  endwhile;

  $args = array(
    'show_all' => false,
    'end_size' => 1,
    'mid_size' => 1,
    'prev_next' => true,
    'prev_text' => __('« Previous'),
    'next_text' => __('Next »'),
    'add_args' => false,
    'add_fragment' => '',
    'screen_reader_text' => __('Posts navigation'),
  );

  paginator(get_pagenum_link());

  wp_reset_query();

else :
  get_template_part('template-parts/section-items/content', 'none');
endif;

/**
 * @return array of parameter for wp query
 */
function defining_parameters()
{
  if (is_category()) {
    $filter = get_queried_object()->name;
    $filter_by = "category_name";
  }

  $search = $_POST['search_string'];
  $filter = $_POST['link'] ?: $filter;
  $filter_by = $_POST['filterBy'] ?: $filter_by;
  $paged = $_POST['paged'];

  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 6,
  );

  if ($search) {
    $args['s'] = $search;
  }

  if ($filter) {
    $args[$filter_by] = $filter;
  }

  if ($paged) {
    $args['paged'] = $paged;
  }

  return $args;
}
