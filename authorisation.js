
/**
 * registration function with api on the another server
 * if response is ok - user should confirm his email,
 * in other way - user get error message
 *
 * @param e event
 */
function registration(e) {
  let userEmailInput = $('#full-email');
  let userRepeatEmailInput = $('#repeat-email');

  let userPhoneInput = $('#full-phone');
  let userPasswordInput = $('#full-password');
  let userRepeatInput = $('#repeat-password');

  let userFirstNInput = $('#first-name');
  let userFirstName = userFirstNInput.val();

  let userLastNInput = $('#last-name');
  let userLastName = userLastNInput.val();
  let userPhone = userPhoneInput.val();

  let subscribed = document.getElementById('subscribed').checked;

  let pass = userPasswordInput.val();
  let passrepeat = userRepeatInput.val();
  let userEmail = getCorrectEmail(userEmailInput.val());
  let repeatUserEmail = getCorrectEmail(userRepeatEmailInput.val());

  if (!validationName(userFirstName)) {
    show_error_inform(userFirstNInput, $('.error-message.first-name'), 'Name should be from 2 to 20 letters')
    return;
  }

  if (!validationName(userLastName)) {
    show_error_inform(userLastNInput, $('.error-message.last-name'), 'Last Name should be from 2 to 20 letters')
    return;
  }

  if (userPhone) {
    if (!validatePhone(userPhone)) {
      show_error_inform(userPhoneInput, $('.error-message.full-phone'), 'Phone should be from 6 to 15 digits')
      return;
    }
  }

  let isValid = validateEmail(userEmail);
  if (!isValid.status) {
    show_error_inform(userEmailInput, $('.error-message.email'), isValid.text)
    return;
  } else {
    if (userEmail !== repeatUserEmail) {
      show_error_inform(userRepeatEmailInput, $('.error-message.repeat-email'), 'Emails are not the same');
      return;
    }
  }

  let validatePassword = validatePass(pass, passrepeat);

  if (validatePassword !== 'true') {
    show_error_inform(userPasswordInput, $('.error-message.repeat-password'), validatePassword);
    return;
  }

  $.ajax({
    type: "POST",
    url: ajaxscript.ajax_url,
    data: {
      action: "show_registration",
      user_first_name: userFirstName,
      user_last_name: userLastName,
      user_email: userEmail,
      tel: userPhone,
      subscribed: subscribed,
      user_pass: pass,
      scope: "user",
    },
    success: function (response) {
      try {
        let resp = JSON.parse(response);
        $('.error-message.form').html(resp['data']);
      } catch (e) {
        if (response === 'success') {
          say_ok_and_go_home("Confirm your email");
        } else {
          window.location.replace(response);
        }
      }
    }
  });
}
