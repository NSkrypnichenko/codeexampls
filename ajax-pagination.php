<?php

function paginator( $first_page_url ){
    global $wp_query;

    // remove the trailing slash if necessary
    $first_page_url = untrailingslashit( $first_page_url );

    // it is time to separate our URL from search query
    $first_page_url_exploded = array();
    $first_page_url_exploded = explode("/?", $first_page_url);
    // by default a search query is empty
    $search_query = '';
    // if the second array element exists
    if( isset( $first_page_url_exploded[1] ) ) {
        $search_query = "/?" . $first_page_url_exploded[1];
        $first_page_url = $first_page_url_exploded[0];
    }

    $posts_per_page = (int) $wp_query->query_vars['posts_per_page'];

    $current_page = (int) $wp_query->query_vars['paged'];
    // the overall amount of pages
    $max_page = $wp_query->max_num_pages;

    // don't have to display pagination or load more button in this case
    if( $max_page <= 1 ) return;

    // set the current page to 1 if not exists
    if( empty( $current_page ) || $current_page == 0) $current_page = 1;

    $links_in_the_middle = 4;
    $links_in_the_middle_minus_1 = $links_in_the_middle-1;

    $first_link_in_the_middle = $current_page - floor( $links_in_the_middle_minus_1/2 );
    $last_link_in_the_middle = $current_page + ceil( $links_in_the_middle_minus_1/2 );

    // some calculations with $first_link_in_the_middle and $last_link_in_the_middle
    if( $first_link_in_the_middle <= 0 ) $first_link_in_the_middle = 1;
    if( ( $last_link_in_the_middle - $first_link_in_the_middle ) != $links_in_the_middle_minus_1 ) { $last_link_in_the_middle = $first_link_in_the_middle + $links_in_the_middle_minus_1; }
    if( $last_link_in_the_middle > $max_page ) { $first_link_in_the_middle = $max_page - $links_in_the_middle_minus_1; $last_link_in_the_middle = (int) $max_page; }
    if( $first_link_in_the_middle <= 0 ) $first_link_in_the_middle = 1;

    // begin to generate HTML of the pagination
    $pagination = '<nav id="pagination" class="navigation pagination" role="navigation"><div class="nav-links">';

    // when to display "..." and the first page before it
    if ($first_link_in_the_middle >= 2 && $links_in_the_middle < $max_page) {
        $pagination.= '<a href="'. $first_page_url . $search_query . '" class="page-numbers">1</a>'; // first page

        if( $first_link_in_the_middle != 2 )
            $pagination .= '<span class="page-numbers extend">...</span>';
    }

    // arrow left (previous page)
    if ($current_page != 1)
        $pagination.= '<a href="'. $first_page_url . '/page/' . ($current_page-1) . $search_query . '" data-linkpage="'.($current_page-1).'" class="prev page-numbers"><</a>';

    // loop page links in the middle between "..." and "..."
    for($i = $first_link_in_the_middle; $i <= $last_link_in_the_middle; $i++) {
        if($i == $current_page) {
            $pagination.= '<span class="page-numbers current">'.$i.'</span>';
        } else {
            $pagination .= '<a href="'. $first_page_url . '/page/' . $i . $search_query .'" data-linkpage="'.$i.'" class="page-numbers">'.$i.'</a>';
        }
    }

    // arrow right (next page)
    if ($current_page != $last_link_in_the_middle )
        $pagination.= '<a href="'. $first_page_url . '/page/' . ($current_page+1) . $search_query .'" data-linkpage="'.($current_page+1).'" class="next page-numbers">></a>';

    // when to display "..." and the last page after it
    if ( $last_link_in_the_middle < $max_page ) {

        if( $last_link_in_the_middle != ($max_page-1) )
            $pagination .= '<span class="page-numbers extend">...</span>';

        $pagination .= '<a href="'. $first_page_url . '/page/' . $max_page . $search_query .'"data-linkpage="'.($current_page+1).'" class="page-numbers">'. $max_page .'</a>';
    }

    // end HTML
    $pagination.= "</div></nav>\n";

//     replace first page before printing it
    echo str_replace(array("/page/1?", "/page/1\""), array("?", "\""), $pagination);
}


/**
 * function-callback for pagination, rerender section with correct query
 */
function ajax_show_posts_by_pagination() {
    get_template_part('template-parts/sections/posts-on-blog-page' );
    wp_die();
}
add_action( 'wp_ajax_pagination', 'ajax_show_posts_by_pagination' );
add_action( 'wp_ajax_nopriv_pagination', 'ajax_show_posts_by_pagination' );

